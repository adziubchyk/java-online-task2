import java.util.Scanner;

public class Party {
    public static void main(String []args){



        Scanner sc = new Scanner(System.in);
        int N=sc.nextInt();
        double res=1;
        int amount=N;
        for(int i=0;i<N-1;i++) {
            res*=(N-i-1.0)/(N-1.0);
            System.out.println("res: "+res);
            if(res<=0.5&&amount>i)
                amount=i;
        }

        System.out.println("probability that everyone at the party hear the rumor: "+res*100.0+"%");
        System.out.println("expected number of people to hear the rumor:"+amount);
    }
}
